import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyBb9Y2v39DebDWuGdBu5vLmnOdULxLonZQ",
  authDomain: "udemy-ninja-smoothies-380c0.firebaseapp.com",
  databaseURL: "https://udemy-ninja-smoothies-380c0.firebaseio.com",
  projectId: "udemy-ninja-smoothies-380c0",
  storageBucket: "udemy-ninja-smoothies-380c0.appspot.com",
  messagingSenderId: "25954264981"
};

const firebaseApp = firebase.initializeApp(config);
//firebaseApp.firestore().settings({ timestampsInSnapshots: true })

// export firestore database

export default firebaseApp.firestore()

