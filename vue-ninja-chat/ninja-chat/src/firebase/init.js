  import firebase from 'firebase'
  import firestore from 'firebase/firestore'
  
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBoxjit71UbN6QlIQtY4E4QNzrsCeu46tQ",
    authDomain: "udemy-ninja-chat-33357.firebaseapp.com",
    databaseURL: "https://udemy-ninja-chat-33357.firebaseio.com",
    projectId: "udemy-ninja-chat-33357",
    storageBucket: "udemy-ninja-chat-33357.appspot.com",
    messagingSenderId: "483484392623"
  };
  const firebaseApp = firebase.initializeApp(config);
  //firebaseApp.firestore().settings({timestampsInSnapshots: true})
  
  export default firebaseApp.firestore()